---
title: "Cuando el software es la ley"
date: 2021-08-30
summary: "Cuando se habla de la blockchain y de las criptomonedas, se habla de algunas grandes ventajas que poseen sobre el sistema tradicional basado en el dinero fiduciario. Una de ellas es que el sistema sobre el que se construyen es criptográficamente seguro, al menos a día de hoy. Esto quiere decir que no hay ninguna forma de acceder a los activos de un usuario mediante fuerza bruta. Sobre el papel, el sistema es infalible e inexpugnable. Y en la práctica también, al menos por ahora."
tags: ["blockchain"]
categories:
  - opinión
---

El problema, una vez más, no viene tanto del software, sino de los humanos, porque los humanos sí que somos falibles, y por tanto, todo lo que construimos también lo es. Y eso sin contar que también hay humanos que actúan de mala fe.

La solución obvia entonces sería quitar de en medio a los humanos. Dejar el poder en manos del software. Pero resulta que el software está hecho por nosotros, los humanos, así que nunca será imparcial ni estará libre de errores. Veamos qué consecuencias tiene esta [trampa](https://es.wikipedia.org/wiki/Trampa_22_(libro)).

Cuando un usuario accede a uno de los *exchages* de criptomonedas (Coinbase, por nombrar el más famoso, pero hay muchos más) compra, por ejemplo, una cierta cantidad de bitcoins a cambio de su dinero fiat. Acto seguido puede optar por transferir esos activos a una cuenta que no está bajo el "control" de Coinbase. A partir de este momento, el usuario tiene que guardar como oro en paño la clave privada con la cual accede a la blockchain. Si la pierde, o si sus fondos se transfieren a otra cuenta de la que no tiene el control, los pierde. Es así precisamente por la naturaleza de la blockchain. No hay ninguna entidad a la que recurrir, no se puede dar macha atrás, no hay nada que hacer. La otra opción era dejarlos en la cuenta de Coinbase, que almacena tu clave privada junto a tu cuenta, y esta cuenta sí, puedes recuperarla aunque hayas perdido tu contraseña si demuestras que es tuya, como en casi todos los sitios web. Pero entonces, ¿qué sentido tiene? Simplemente estamos deshaciéndonos de nuestro banco tradicional para echarnos en los brazos de algo que hace a grandes rasgos cosas parecidas a un banco.

Ahora supongamos el siguiente ataque: te descargas una aplicación para gestionar tu cartera de criptomonedas, con tan mala suerte, que procede de una web que ha sido clonada de la original, con un dominio casi idéntico. Es decir, phishing. No has comprobado la integridad del binario y la instalas sin más. El único detalle es que esa aplicación tiene una puerta trasera que hace que, en el momento que accedes a tus fondos con tu clave privada, se efectúa por debajo, sin hacer ruido una operación que transfiere una parte o todos tus activos a otra cuenta. La del atacante, claro.

Pero no nos volvamos tan rebuscados. Supongamos simplemente un error del usuario. Si vas a hacer una transferencia e introduces erróneamente la dirección de blockchain del destinatario, no hay vuelta atrás.

De hecho, hay una proporción enorme de criptomonedas que no tienen dueño, en el sentido de que se perdió la clave privada para acceder a ellas o se transfirieron a cuentas sin dueño. Con lo cual nunca se podrán recuperar. Nunca.

Y es que, tanto si se trata de un accidente como de un robo, no hay nada que hacer. No hay ninguna institución de confianza a la que poder acudir. Pero tampoco hay ninguna forma tecnológica de recuperarlo. Porque es así por diseño. Para lo bueno y para lo malo.

Pero sí que hay una forma de reducir al mínimo este riesgo. Se puede contratar a alguien experto en finanzas y tecnología que maneje nuestros activos. Y si mete la pata y pierde la clave privada, o es víctima de un ataque, que responda con su propio capital. Vaya... parece que acabamos de reinventar el banco.

No seré yo quien niegue la belleza tecnológica y filosófica del diseño de la blockchain. Su concepción es perfecta. Si asumimos que los humanos y nuestras instituciones humanas somos imperfectos, quitémonos de la ecuación. Tanto los bancos comerciales como los bancos centrales han hecho, y siguen haciendo, auténticas barbaridades. Pero la pregunta es, como sociedad, ¿estamos dispuestos a operar bajo estas nuevas reglas? Cuando el software es la ley, no hay posibilidad de revocación. No hay segundas oportunidades.

Esto es lo que ocurre cuando nos dejamos seducir por la blockchain de forma acrítica y no somos conscientes de todo lo que hay detrás. Primero aseguramos que todas las instituciones que hemos creado a lo largo de los siglos están obsoletas. Lo cual nos empuja a deshacernos de ellas, para así ir poco a poco entendiendo, con cada problema que surge, por qué eran necesarias. Y entonces construirlas de nuevo adaptadas al nuevo entorno. Puede que realmente necesitemos reinventarlas y adaptarlas a los nuevos tiempos. Pero también corremos el riesgo de que acaben siendo más de lo mismo, solo que más grandes, menos controladas, y por tanto, más corruptas.
